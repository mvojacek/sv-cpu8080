// 3461BS pinout:
//
//  D1  a   f   D2  D3  b
//  e   d   dp  c   g   D4
//
// OMDAZZ pinout:
//
// 137 128 126 136 135 121
// 132 129 127 125 124 133
// 
module digits_3461BS (
input wire clk, // 50MHz input clock

input wire [15:0] val,
input wire [3:0]  ena,
input wire [3:0]  dp,

//
// OMDAZZ Mapping:
//  3   2   1   0
//  D1  D2  D3  D4
// 137 136 135 133
// 
output reg [3:0] D, // DIGIT selector, PULL HIGH
//
//    0    |    a
// 5     1 | f     b
//    6    |    g
// 4     2 | e     c
//    3    |    d
//
// OMDAZZ Mapping:
//  6   5   4   3   2   1   0   dp
//  g   f   e   d   c   b   a   dp
// 124 126 132 129 125 121 128  127
//
output reg [6:0] S, // SEGMENTS output, PULL LOW
output reg DP // PULL LOW
);

reg [1:0] cnt_reg;

initial begin
   cnt_reg <= 2'h0;
end

always @(posedge clk) begin
   cnt_reg <= cnt_reg + 1;
end

wire [1:0] cnt = cnt_reg[1:0];

wire [6:0] s1;
wire [6:0] s2;
wire [6:0] s3;
wire [6:0] s4;

nibble2segment ns1(.nibble(val[15:12]), .segment(s1));
nibble2segment ns2(.nibble(val[11:8]),  .segment(s2));
nibble2segment ns3(.nibble(val[7:4]),   .segment(s3));
nibble2segment ns4(.nibble(val[3:0]),   .segment(s4));

always @(*) begin
   DP = ~dp[cnt];
   D = 4'b1111;
   D[cnt] = ~ena[cnt];
   case (cnt)
      0 : S = ~s4;
      1 : S = ~s3;
      2 : S = ~s2;
      3 : S = ~s1;
   endcase
end

endmodule

module nibble2segment (
   input wire [3:0] nibble,
   output reg [6:0] segment
);

//
//    0    |    a
// 5     1 | f     b
//    6    |    g
// 4     2 | e     c
//    3    |    d
//
always @(*) begin
   case (nibble)
      4'b0000 : segment = 7'b0111111;
      4'b0001 : segment = 7'b0000110;
      4'b0010 : segment = 7'b1011011;
      4'b0011 : segment = 7'b1001111;
      4'b0100 : segment = 7'b1100110;          
      4'b0101 : segment = 7'b1101101;
      4'b0110 : segment = 7'b1111101;
      4'b0111 : segment = 7'b0000111;
      
      4'b1000 : segment = 7'b1111111;
      4'b1001 : segment = 7'b1101111;
      4'b1010 : segment = 7'b1110111;
      4'b1011 : segment = 7'b1111100;
      4'b1100 : segment = 7'b0111001;
      4'b1101 : segment = 7'b1011110;
      4'b1110 : segment = 7'b1111001;
      4'b1111 : segment = 7'b1110001;
   endcase
end

endmodule



