`define PROBE 1

module memory(
   input clock,

   input we, re,
   
   input [15:0] addr,
   input [7:0] in,
   
   output [7:0] out
);

reg re_sync = 0;

always @(posedge clock) begin
   re_sync <= re;
end

wire [7:0] ram_out;
assign out = re_sync ? ram_out : 8'bZ;

`ifdef PROBE
wire ram_clock = probe_override_clk ? probe_clk : clock;
wire ram_we = probe_override ? probe_we : we;
wire ram_re = probe_override ? probe_re : re;
wire [7:0] ram_in = probe_override ? probe_in : in;
wire [15:0] ram_addr = probe_override ? probe_addr : addr;
`else
wire ram_clock = clock;
wire ram_we = we;
wire ram_re = re;
wire [7:0] ram_in = in;
wire [15:0] ram_addr = addr;
`endif

cpuram cpuram(
   .address(ram_addr),
   .clock(ram_clock),
   .data(ram_in),
   .rden(ram_re),
   .wren(ram_we),
   .q(ram_out)
);

`ifdef PROBE
wire probe_override = probe_source[0];
wire probe_clk = probe_source[1];
wire probe_re = probe_source[2];
wire probe_we = probe_source[3];
wire probe_override_clk = probe_source[4];
wire [7:0] probe_in = probe_source[11:4];
wire [15:0] probe_addr = probe_source[27:12];

wire  [29:0] probe_source;
wire [39:0] probe_probe = {
   ram_in[7:0], // 34:27
   ram_addr[15:0], // 26:11
   ram_out[7:0], // 10:3,
   ram_we, // 2
   ram_re, //1
   ram_clock, //0
};

ramprobe ramprobe(
   .source(probe_source),
   .probe(probe_probe)
// .source_clk(~in_clock)
);
`endif

endmodule
