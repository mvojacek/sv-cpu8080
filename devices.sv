`define PROBE 1

module devices(
   input clock,
   input seg7_plex_clock,
	input int_clock,
	input [15:0] rtcCounter,

   input  [7:0] addr,
   inout  [7:0] data,
   
   input read, write,
	
	input  cpu_inta,
	output cpu_intr,

   input  [3:0] board_switches,
	input        board_infra,
   
   output [3:0] board_3461BS_D,
   output [6:0] board_3461BS_S,
   output       board_3461BS_DP,
   
   output [3:0] board_leds,
   output       board_buzzer
);

// 7 Segment

wire seg7_wr_hi, seg7_wr_lo, seg7_wr_crl;
wire [7:0] seg7_dbg_hi, seg7_dbg_lo, seg7_dbg_crl;

seg7 seg7_inst(
   .clock(clock),
   .plex_clock(seg7_plex_clock),
   .data(data),
   .wr_hi(seg7_wr_hi),
   .wr_lo(seg7_wr_lo),
   .wr_crl(seg7_wr_crl),

   .board_3461BS_D(board_3461BS_D),
   .board_3461BS_S(board_3461BS_S),
   .board_3461BS_DP(board_3461BS_DP),
   
   .hi(seg7_dbg_hi),
   .lo(seg7_dbg_lo),
   .crl(seg7_dbg_crl)
);

// LEDs

wire leds_read, leds_write;
wire [3:0] leds_dbg_buffer;

leds leds_inst(
   .clock(clock),
   .data(data),
   .read(leds_read),
   .write(leds_write),
   
   .board_leds(board_leds),
   
   .buffer(leds_dbg_buffer)
);

// Switches

wire switches_read;
wire [3:0] switches_dbg_buffer;

switches switches_inst(
   .clock(clock),
   .data(data),
   .read(switches_read),
   
   .board_switches(board_switches),
   
   .buffer(switches_dbg_buffer)
);

// Buzzer

wire buzzer_wr_ena, buzzer_wr_freq;
wire buzzer_dbg_enable;
wire [7:0] buzzer_dbg_frequency;

buzzer buzzer_inst(
	.clock(clock),
	.rtcCounter(rtcCounter),
	
	.data(data),
	.write_ena(buzzer_wr_ena),
	.write_freq(buzzer_wr_freq),
	
	.board_buzzer(board_buzzer),
	
	.enable(buzzer_dbg_enable),
	.frequency(buzzer_dbg_frequency)
);

// Interrupt manager

wire [1:0] int_addr;
wire int_read, int_write;
wire [7:0] int_input = {4'b0, ~board_switches};
wire [7:0] int_dbg_chg_input, int_dbg_int_done_q, int_dbg_int_done_s;
wire [2:0] int_dbg_state;

interrupt_manager int_mgr(
	.clock(clock),
	.int_clock(int_clock),
	.data(data),
	.addr(int_addr),
	
	.write(int_write),
	.read(int_read),
	
	.cpu_inta(cpu_inta),
	.cpu_intr(cpu_intr),
	
	.int_input(int_input),
	
	.chg_input(int_dbg_chg_input),
	.int_done_q(int_dbg_int_done_q),
	.int_done_s(int_dbg_int_done_s),
	.state(int_dbg_state)
);

parameter
   io_seg7lo = 8'h00,
   io_seg7hi = 8'h01,
   io_seg7crl = 8'h02,
   io_leds = 8'h03,
   io_switches = 8'h04,
	io_buzzer = 8'h05,
	io_buzzer_freq = 8'h06,
	io_xx = 8'h07,
	io_intmgr_prefix = 8'b000010xx, // 0x8 - 0xB
	io_xx2 = 8'h0C
;
	

always_comb begin
   seg7_wr_hi = 0;
   seg7_wr_lo = 0;
   seg7_wr_crl = 0;
   leds_read = 0;
   leds_write = 0;
   switches_read = 0;
	buzzer_wr_ena = 0;
	buzzer_wr_freq = 0;
	int_addr = 0;
	int_read = 0;
	int_write = 0;
   
   unique casex (addr)
      io_seg7lo:
         seg7_wr_lo = write;
      io_seg7hi:
         seg7_wr_hi = write;
      io_seg7crl:
         seg7_wr_crl = write;
      io_leds: begin
         leds_read = read;
         leds_write = write;
      end
      io_switches:
         switches_read = read;
		io_buzzer:
			buzzer_wr_ena = write;
		io_buzzer_freq:
			buzzer_wr_freq = write;
		io_intmgr_prefix: begin
			reg [2:0] entry;
			entry = addr[2:0];
			
			int_addr = entry;
			int_read = read;
			int_write = write;
		end
   endcase
end

`ifdef PROBE
wire [127:0] probe_probe = {
	int_clock, //97
	cpu_inta, //96
	cpu_intr, //95
	int_dbg_state[2:0],//94:92
	int_dbg_int_done_q[7:0], //91:84
	int_dbg_int_done_s[7:0], //83:76
	int_dbg_chg_input[7:0],  //75:68
	buzzer_dbg_frequency[7:0], //67:60
	buzzer_dbg_enable,	//59
	buzzer_wr_freq,		//58
	buzzer_wr_ena,			//57
   switches_dbg_buffer[3:0], //56:53
   leds_dbg_buffer[3:0],//52:49
   seg7_dbg_crl[7:0],  //48:41
   seg7_dbg_hi[7:0],   //40:33
   seg7_dbg_lo[7:0],   //32:25
   seg7_wr_hi,         //24
   seg7_wr_lo,         //23
   seg7_wr_crl,        //22
   leds_read,          //21
   leds_write,         //20
   switches_read,      //19
   data[7:0],          //18:11
   addr[7:0],          //10:3
   write,              //2
   read,               //1
   clock,              //0
};

devprobe devprobe(
// .source(probe_source),
   .probe(probe_probe)
// .source_clk(~in_clock)
);
`endif

endmodule

// ======
// 7 segment
// ======

module seg7(
   input clock, plex_clock,
   input [7:0] data,
   input wr_hi, wr_lo, wr_crl,
   
   output [3:0] board_3461BS_D,
   output [6:0] board_3461BS_S,
   output       board_3461BS_DP,
   
   output reg [7:0] hi, lo, crl //output for debugging
);

//reg [7:0] hi, lo, crl;

always @(posedge clock) begin
   if (wr_hi)
      hi <= data;
   if (wr_lo)
      lo <= data;
   if (wr_crl)
      crl <= data;
end

digits_3461BS digits_3461BS (
   .clk(plex_clock),
   .val({hi,lo}),
   .ena(crl[3:0]),
   .dp(crl[7:4]),
   
   .D(board_3461BS_D),
   .S(board_3461BS_S),
   .DP(board_3461BS_DP)
);

endmodule

// ======
// LEDs
// ======

module leds(
   input clock,
   inout [7:0] data,
   input read, write,
   
   output [3:0] board_leds,
   
   output reg [3:0] buffer // output for debug
);

assign board_leds = ~buffer;

wire read_sync;
assign data = read_sync ? {4'h0, buffer} : 8'bZ;

always @(posedge clock) begin
   read_sync <= read;
   if (write)
      buffer <= data[3:0];
end

endmodule

// ======
// Switches
// ======

module switches(
   input clock,
   output [7:0] data,
   input read,
   
   input  [3:0] board_switches,
	
   output reg [3:0] buffer // output for debug
);

wire read_sync;
assign data = read_sync ? {4'h0, buffer} : 8'bZ;

always @(posedge clock) begin
   read_sync <= read;
   buffer <= ~board_switches;
end

endmodule

// ======
// Buzzer
// ======

// 0: 32k
// 1: 16k
// 2: 8k
// 3: 4k
// 4: 2k
// 5: 1k
// 6: 512
// 7: 256
// 8: 128
// 9: 60

module buzzer(
	input clock,
	input [15:0] rtcCounter,
	
	input [7:0] data,
	input write_ena, write_freq,
	
	output board_buzzer,
	
	output reg enable, // ouput for debug
	output reg [7:0] frequency
);

always @(posedge clock) begin
	if (write_ena)
		enable <= data[0];
	if (write_freq)
		frequency <= data;
end

always @(*) begin
	board_buzzer = enable & rtcCounter[frequency[3:0]];
end

endmodule

// ======
// Interrupt manager
// ======

// entry: vec1[2:0] @ ena1 @ vec0[2:0] @ ena0
// 4 entries = 8 interrupt inputs

module interrupt_manager(
	input clock,
	input int_clock,
	
	inout [7:0] data,
	
	input [1:0] addr,
	input write, read,
	
	input cpu_inta,
	output reg cpu_intr,
	
	input [7:0] int_input,
	
	output reg [7:0] chg_input, //debugging
	output [7:0] int_done_q, int_done_s,
	output reg [2:0] state
);

reg [3:0] entries [7:0];
`define entries_at(adr) {entries[{adr,1'b1}], entries[{adr,1'b0}]}
`define entries_enables {entries[7][0],entries[6][0],entries[5][0],entries[4][0],entries[3][0],entries[2][0],entries[1][0],entries[0][0]}

reg read_sync = 0;
reg jam_bus = 0;
reg [7:0] jam_data;
assign data = jam_bus ? jam_data : (read_sync ? `entries_at(addr) : 8'bZ);

always @(posedge clock) begin
	read_sync <= read;
	if (write)
		`entries_at(addr) <= data;
end

reg [7:0] prev_input;

wire  [7:0] int_done_r;
sr8 sr_int_done(
	.s(int_done_s), .r(int_done_r), .q(int_done_q)
);

assign int_done_r = {8{~int_clock}};

always @(posedge int_clock) begin
	chg_input &= ~int_done_q;
	
	chg_input |= int_input ^ prev_input;
	chg_input &= `entries_enables;
	
	prev_input <= int_input;
end

wire int_active = |chg_input;

parameter
	s_noop = 0,
	s_req = 1,
	s_ack = 2,
	s_fin = 3;

always @(*) begin
	jam_bus = 0;
	int_done_s = 0;

	if (int_active) begin
		reg [2:0] entry;
		priority casex(chg_input)
			8'b10000000: entry = 7;
			8'bx1000000: entry = 6;
			8'bxx100000: entry = 5;
			8'bxxx10000: entry = 4;
			8'bxxxx1000: entry = 3;
			8'bxxxxx100: entry = 2;
			8'bxxxxxx10: entry = 1;
			8'bxxxxxxx1: entry = 0;
		endcase
		
		unique case (state)
			s_noop: begin // start interrupt handling
				cpu_intr = 0;
				state = s_req;
			end
			s_req: begin // wait for acknowledge
				cpu_intr = 1;
				if (cpu_inta) begin
					state = s_ack; // acknowledged
				end else begin
					state = s_req; // wait
				end
			end
			s_ack: begin // send instruction, wait for ~ack
				cpu_intr = 1;
				jam_data = {2'b11, entries[entry][3:1], 3'b111}; // appropriate RST insn
				jam_bus = 1;
				if (cpu_inta) begin
					state = s_ack;
				end else begin
					state = s_fin;
				end
			end
			s_fin: begin // stay in this state until sync part clears changes
				jam_bus = 0;
				cpu_intr = 0;
				int_done_s[entry] = 1;
				if (chg_input[entry]) begin
					state = s_fin;
				end else begin
					state = s_noop;
				end
			end
			default: begin
				cpu_intr = 0;
				state = s_noop;
			end
		endcase
	end else begin
		cpu_intr = 0;
		state = s_noop;
	end
end

endmodule

module sr8(
	input  [7:0] s, r,
	output [7:0] q
);
`define e(i) if (s[i]) q[i] = 1; else if (r[i]) q[i] = 0;
	always_latch begin
		`e(0) `e(1) `e(2) `e(3) `e(4) `e(5) `e(6) `e(7) 
	end
	
endmodule
