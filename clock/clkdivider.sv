`define PROBE 1

module clkdivider(
   input clk50M,
   
   output clk1M, clkRTC, clk1H, clk1300M, clkCPU, clkPlex, clkInt,
	
	output [15:0] rtcCounter
);

cpupll cpupll(
   .inclk0(clk50M), // 50 MHz
   .c0(clk1M),      // 1MHz
   .c1(clkRTC),     // 32.768KHz
   .c2(clk1300M)    // 1.3GHz
);

reg [15:0] counter;

always @(posedge clkRTC) begin
   counter <= counter + 1;
end

assign rtcCounter = counter;

assign clk1H = counter[15];

assign clkCPU_def = counter[0]; //4Hz @ 13, 32Hz @ 10, 256Hz @ 7, 32KHz @ 0
assign clkPlex_def = counter[4]; //1024Hz
assign clkInt_def = clk1M;


`ifdef PROBE
assign clkCPU = probe_ovr_cpu ? counter[probe_cpu] : clkCPU_def;
assign clkPlex = probe_ovr_plex ? counter[probe_plex] : clkPlex_def;
assign clkInt = probe_ovr_int ? counter[probe_int] : clkInt_def;
`else
assign clkCPU = clkCPU_def;
assign clkPlex = clkPlex_def;
assign clkInt = clkInt_def;
`endif

`ifdef PROBE
wire probe_ovr_plex = probe_source[0];
wire [3:0] probe_plex = probe_source[4:1];
wire probe_ovr_cpu = probe_source[5];
wire [3:0] probe_cpu = probe_source[9:6];
wire probe_ovr_int = probe_source[10];
wire [3:0] probe_int = probe_source[14:11];

wire  [31:0] probe_source;
wire [7:0] probe_probe = {
   8'h00
};

clkprobe clkprobe(
   .source(probe_source),
   .probe(probe_probe)
);
`endif

endmodule
