
module clkprobe (
	probe,
	source);	

	input	[7:0]	probe;
	output	[31:0]	source;
endmodule
