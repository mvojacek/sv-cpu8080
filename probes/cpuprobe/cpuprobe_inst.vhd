	component cpuprobe is
		port (
			probe  : in  std_logic_vector(191 downto 0) := (others => 'X'); -- probe
			source : out std_logic_vector(15 downto 0)                      -- source
		);
	end component cpuprobe;

	u0 : component cpuprobe
		port map (
			probe  => CONNECTED_TO_probe,  --  probes.probe
			source => CONNECTED_TO_source  -- sources.source
		);

