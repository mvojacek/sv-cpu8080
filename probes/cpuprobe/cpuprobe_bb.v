
module cpuprobe (
	probe,
	source);	

	input	[191:0]	probe;
	output	[15:0]	source;
endmodule
