
module ramprobe (
	probe,
	source);	

	input	[39:0]	probe;
	output	[29:0]	source;
endmodule
