	component ramprobe is
		port (
			probe  : in  std_logic_vector(39 downto 0) := (others => 'X'); -- probe
			source : out std_logic_vector(29 downto 0)                     -- source
		);
	end component ramprobe;

	u0 : component ramprobe
		port map (
			probe  => CONNECTED_TO_probe,  --  probes.probe
			source => CONNECTED_TO_source  -- sources.source
		);

