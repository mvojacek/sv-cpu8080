
module devprobe (
	source,
	probe);	

	output	[31:0]	source;
	input	[127:0]	probe;
endmodule
