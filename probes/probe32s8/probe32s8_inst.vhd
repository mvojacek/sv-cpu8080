	component probe32s8 is
		port (
			source : out std_logic_vector(7 downto 0);                     -- source
			probe  : in  std_logic_vector(31 downto 0) := (others => 'X')  -- probe
		);
	end component probe32s8;

	u0 : component probe32s8
		port map (
			source => CONNECTED_TO_source, -- sources.source
			probe  => CONNECTED_TO_probe   --  probes.probe
		);

