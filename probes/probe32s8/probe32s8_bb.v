
module probe32s8 (
	source,
	probe);	

	output	[7:0]	source;
	input	[31:0]	probe;
endmodule
