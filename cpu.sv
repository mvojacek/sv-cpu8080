`define PROBE 1

module cpu(
      input in_clock, in_reset,
      
      output reg [15:0] addr,
      inout [7:0] data,
      
      output reg readmem,
      output reg writemem,
      
      output reg readio,
      output reg writeio,
		
		input intr,
		output reg inta,
		
		input hold,
		output reg holda
);

`ifdef PROBE
wire reset = probe_override_reset ? probe_s_reset : in_reset;
wire clock = probe_override_clock ? probe_s_clock : in_clock;
`else
wire reset = in_reset;
wire clock = in_clock;
`endif

// Registers

//runtime

reg [15:0] pc;
reg [15:0] sp;
reg [7:0]  opcode;

reg inte, intcyc, intcyc_fetchimm;

//macros

reg [7:0] arg;
reg [7:0] argh;
reg [7:0] res;
reg [7:0] resh;
reg [7:0] temp;
reg [7:0] temph;

`define arg16 {argh,arg}
`define res16 {resh,res}
`define temp16 {temph,temp}

//usermode

enum bit [2:0] {
   rB, rC, rD, rE, rH, rL, rM, rA
} reg_enum;

reg [7:0] regs [0:7];

`define rB regs[rB]
`define rC regs[rC]
`define rD regs[rD]
`define rE regs[rE]
`define rH regs[rH]
`define rL regs[rL]
`define rA regs[rA]

//pairs

enum bit [1:0] {
   rBC, rDE, rHL, rSP
} wreg_enum;

`define rBC {`rB,`rC}
`define rDE {`rD,`rE}
`define rHL {`rH,`rL}
`define rSP sp
`define rPSW {`rA, flag}

//flag

reg [7:0] flag;

enum bit [2:0] {
   fC, fP = 3'd2, fA = 3'd4, fZ = 3'd6, fS
} flag_values;

`define fC flag[fC] // carry
`define fP flag[fP] // parity (1=even)
`define fA flag[fA] // aux carry
`define fZ flag[fZ] // zero
`define fS flag[fS] // sign

// ALU

reg [2:0] alu_op;
reg [7:0] alu_a;
reg [7:0] alu_b;

reg alu_cin;

wire [7:0] alu_out;

wire alu_car, alu_zer, alu_sgn, alu_par, alu_axc;

alu alu(
   .op(alu_op),
   .a(alu_a),
   .b(alu_b),
   
   .cin(alu_cin),

   .ena(1),
   .out(alu_out),
   
   .car(alu_car),
   .zer(alu_zer),
   .sgn(alu_sgn),
   .par(alu_par),
   .axc(alu_axc)
);

// Controls

reg [7:0] data_out;
reg data_out_ena;
assign data = data_out_ena ? data_out : 7'bZ;

// State machine

parameter // 0-63
   s_idle = 0,
   s_fetchi = 1,
   s_halt = 2,
   s_exec = 3,
   m_fetch = 4,
   m_fetch2 = 5,
	m_store = 6,
	m_store2 = 7;

`define entrypoint_s s_fetchi

(* syn_encoding = "user" *) reg [5:0] state;
(* syn_encoding = "user" *) reg [1:0] step;

(* syn_encoding = "user" *) reg [5:0] state_stack;
(* syn_encoding = "user" *) reg [1:0] step_stack;

`define switch(ns) begin step <= 0; state <= ns; end
`define pushn(st, nstep) begin state_stack <= state; step_stack <= nstep; `switch(st); end
`define push(st) `pushn(st, step + 1);
`define pop begin state <= state_stack; step <= step_stack; end
`define next step <= step + 1;
`define incpc pc <= pc + 1;
`define bincpc pc = pc + 1;
`define entrypoint `switch(`entrypoint_s);
`define halt `switch(s_halt)
`define nexti begin `incpc; `entrypoint; end
`define m_fetch_imm  begin if (!intcyc) begin `bincpc; `arg16 = pc;          end intcyc_fetchimm <= intcyc; `push(m_fetch);  end
`define m_fetch_imm2 begin if (!intcyc) begin `bincpc; `arg16 = pc; `bincpc; end intcyc_fetchimm <= intcyc; `push(m_fetch2); end

always @(posedge clock or posedge reset) begin
   if (reset) begin
      state <= s_idle;
      step <= 0;
      state_stack <= 0;
      step_stack <= 0;
      pc <= 0;
      opcode <= 0;
		inte <= 0;
		intcyc <= 0;
		intcyc_fetchimm <= 0;
		inta <= 0;
		holda <= 0;
		
      `rBC <= 0;
      `rDE <= 0;
      `rHL <= 0;
      `rSP <= 0;
      `arg16 <= 0;
      `temp16 <= 0;
      `res16 <= 0;
      addr <= 0;
		
   end else unique case (state)
      
      s_idle: `switch(s_fetchi)
      
      s_fetchi: begin
			if (step == 0) begin
				if (intr & inte) begin
					intcyc <= 1;
					inte <= 0;
					inta <= 1;
				end else begin
					intcyc <= 0;
					readmem <= 1;
					addr <= pc;
				end
				`next
			end else begin
				if (intcyc) begin
					unique case (step)
						1: `next // wait
						2: begin
							opcode <= data;
							inta <= 0;
							`switch(s_exec)
						end
						default: `halt
					endcase
				end else begin
					unique case (step)
						1: begin
							opcode <= data;
							readmem <= 0;
							`switch(s_exec)
						end
						default: `halt
					endcase
				end
			end
      end
      
      s_exec: begin     
         unique case (opcode[7:6])
            2'b00: begin //data transfers and logic
               unique casex (opcode[5:0])
                  6'b000000: begin // nop
                     `nexti
                  end
                  
						6'b0xx111: begin // rlc,ral,rrc,rar
							reg [1:0] op;
							op = opcode[4:3];
							
							unique case (op)
								2'b00: {`fC, `rA} <= {`rA, `rA[7]}; //rlc
								2'b01: {`rA, `fC} <= {`rA[0], `rA}; //rrc
								2'b10: {`fC, `rA} <= {`rA, `fC}; //ral
								2'b11: {`rA, `fC} <= {`fC, `rA}; //rar
							endcase
							`nexti
						end
						
						6'b111111: begin // cmc
							`fC <= ~`fC;
							`nexti
						end
						
						6'b110111: begin // stc
							`fC <= 1;
							`nexti
						end
						
						6'b101111: begin // cma
							`rA <= ~`rA;
							`nexti
						end
							
						6'b100111: begin // daa
							unique case (step)
								0: begin
									if (`rA[3:0] > 9 || `fA)
										{`fA, `rA} <= `rA + 8'h06;
									`next
								end
								1: begin
									if (`rA[7:4] > 9 || `fC)
										{`fC, `rA} <= `rA + 8'h60;
									`nexti
								end
								default: `halt
							endcase
						end
						
						6'bxx1001: begin // dad
							reg [1:0] src;
							src = opcode[5:4];
							
							unique case (src)
								rBC: {`fC, `rHL} <= `rHL + `rBC;
								rDE: {`fC, `rHL} <= `rHL + `rDE;
								rHL: {`fC, `rHL} <= `rHL + `rHL;
								rSP: {`fC, `rHL} <= `rHL + `rSP;
							endcase
							`nexti
						end
						
						6'bxxx011: begin // inx,dcx
							reg [1:0] src;
							reg dec;
							src = opcode[5:4];
							dec = opcode[3];
							
							if (dec) begin
								unique case (src)
									rBC: `rBC <= `rBC - 1;
									rDE: `rDE <= `rDE - 1;
									rHL: `rHL <= `rHL - 1;
									rSP: `rSP <= `rSP - 1;
								endcase
							end else begin
								unique case (src)
									rBC: `rBC <= `rBC + 1;
									rDE: `rDE <= `rDE + 1;
									rHL: `rHL <= `rHL + 1;
									rSP: `rSP <= `rSP + 1;
								endcase
							end
							`nexti
						end
						
						6'b1xx010: begin // sta,lda,shld,lhld
							reg [1:0] op;
							op = opcode[4:3];
							
							unique case (step)
								0: `m_fetch_imm2
								1: begin
									`arg16 <= `res16;
									unique case (op)
										2'b10: begin //sta
											res <= `rA;
											`push(m_store)
										end
										2'b11: begin //lda
											`push(m_fetch)
										end
										2'b00: begin //shld
											`res16 <= `rHL;
											`push(m_store2)
										end
										2'b01: begin //lhld
											`push(m_fetch2)
										end
									endcase
								end
								2: begin
									if (op == 2'b11) //lda
										`rA <= res;
									else if (op == 2'b01) //lhld
										`rHL <= `res16;
									`nexti
								end
								default: `halt
							endcase
						end
						
                  6'bxxx10x: begin // inr,dcr
                     reg [2:0] target;
                     reg subtract;
							reg [7:0] resi;
                     target = opcode[5:3];
                     subtract = opcode[0];
                     
                     if (subtract) begin
                        resi = regs[target] - 1;
								`fA <= (regs[target][3:0] - 1) >> 4;
                     end else begin
                        resi = regs[target] + 1;
								`fA <= (regs[target][3:0] + 1) >> 4;
							end
                     
							`fZ = ~|resi;
							`fS = resi[7];
							`fP = ~^resi;
							regs[target] = resi;
							
                     `nexti
                  end
                  
                  6'b0xx010: begin // stax/ldax
                     reg load;
                     reg d_pair;
                     load = opcode[3];
                     d_pair = opcode[4];
                     
                     unique case (step)
                        0: begin
                           `arg16 <= d_pair ? `rDE : `rBC;
                           if (load) begin
                              `push(m_fetch)
                           end else begin
                              res <= regs[rA];
                              `push(m_store)
                           end
                        end
                        1: begin
                           if (load) begin
                              regs[rA] <= res;
                           end
                           `nexti
                        end
                        default: `halt
                     endcase
                  end
                  
                  6'bxxx110: begin // mvi
                     reg [2:0] target;
                     target = opcode[5:3];
                     
                     unique case (step)
                        0: `m_fetch_imm
                        1: begin
                           regs[target] <= res;
                           `nexti
                        end
                        default: `halt
                     endcase
                  end
                  
                  6'bxx0001: begin // lxi
                     reg [1:0] tpair;
                     tpair = opcode[5:4];
                     
                     unique case (step)
                        0: `m_fetch_imm2
                        1: begin
									unique case (tpair)
										rBC: `rBC <= `res16;
										rDE: `rDE <= `res16;
										rHL: `rHL <= `res16;
										rSP: `rSP <= `res16;
									endcase
									`nexti
                        end
                        default: `halt
                     endcase
                  end
                  default: `halt
               endcase
            end
            
            2'b01: begin // mov, hlt
               if (opcode[5:0] == 6'b110110) // hlt
                     `switch(s_halt)
					else begin // mov
						reg [2:0] dst, src;
						reg memsrc, memdst;
						dst = opcode[5:3];
						src = opcode[2:0];
						memsrc = src == rM;
						memdst = dst == rM;
						
						unique case (step)
							0: begin
								
								if (memsrc || memdst) begin
									`arg16 <= `rHL;
									if (memsrc) begin // mem to reg
										`push(m_fetch)
									end else begin // reg to mem
										res <= regs[src];
										`push(m_store)
									end
								end else begin // reg to reg
									regs[dst] <= regs[src];
									`nexti
								end
							end
							1: begin
								if (memsrc) begin
									regs[dst] <= res;
								end
								`nexti
							end
							default: `halt
						endcase
					end
            end
				
				2'b10: begin //alu - add,adc,sub,sbb,ana,xra,ora,cmp
					reg [2:0] op;
					reg [2:0] src;
					op = opcode[5:3];
					src = opcode[2:0];
					
					unique case (step)
						0: begin
							if (src == rM) begin
								`arg16 <= `rHL;
								`push(m_fetch)
							end else begin
								`next
							end
						end
						1: begin
							alu_op <= op;
							alu_cin <= `fC;
							alu_a <= `rA;
							if (src == rM) begin
								alu_b <= res;
							end else begin
								alu_b <= regs[src];
							end
							`next
						end
						2: begin
							`rA <= alu_out;
							`fC <= alu_car;
							`fZ <= alu_zer;
							`fS <= alu_sgn;
							`fP <= alu_par;
							`fA <= alu_axc;
							`nexti
						end
						default: `halt
					endcase
				end
            
            2'b11: begin //misc - jmp, call, io...
               unique casex (opcode[5:0])
						6'b11x011: begin // ei,di
							inte <= opcode[3];
							`nexti
						end
						
						6'bxx0101: begin // push
							reg [1:0] pair;
							pair = opcode[5:4];
							
							unique case (step)
								0: begin
									sp = sp - 2;
									`arg16 = sp;
									
									unique case (pair)
										rBC: `res16 <= `rBC;
										rDE: `res16 <= `rDE;
										rHL: `res16 <= `rHL;
										rSP: `res16 <= `rPSW;
									endcase
									
									`push(m_store2)
								end
								1: `nexti
								default: `halt
							endcase
						end
						
						6'bxx0001: begin // pop
							reg [1:0] pair;
							pair = opcode[5:4];
							
							unique case (step)
								0: begin
									`arg16 = sp;
									`push(m_fetch2)
								end
								1: begin
									sp <= sp + 2;
									
									unique case (pair)
										rBC: `rBC  <= `res16;
										rDE: `rDE  <= `res16;
										rHL: `rHL  <= `res16;
										rSP: `rPSW <= `res16;
									endcase
									
									`nexti
								end
								default: `halt
							endcase
						end
						
						6'b101011: begin // xchg
							`rHL <= `rDE;
							`rDE <= `rHL;
							`nexti
						end
						
						6'b100011: begin // xthl
							unique case (step)
								0: begin
									`arg16 <= sp;
									`push(m_fetch2)
								end
								1: begin
									`rHL <= `res16;
									`res16 <= `rHL;
									`push(m_store2)
								end
								2: `nexti
								default: `halt
							endcase
						end
						
						6'b111001: begin // sphl
							`rSP <= `rHL;
							`nexti
						end
						
                  6'b01x011: begin // in/out 
                     reg in;
                     in = opcode[3];
                     
                     unique case (step)
                        0: `m_fetch_imm
                        1: begin
                           addr <= {8'h0, res};
                           if (in) begin
                              readio <= 1;
                           end else begin
                              data_out <= `rA;
                              data_out_ena <= 1;
                              writeio <= 1;
                           end
                           `next
                        end
                        2: begin
                           if (in) begin
                              `rA <= data;
                              readio <= 0;
                           end else begin
                              data_out_ena <= 0;
                              writeio <= 0;
                           end
                           `nexti
                        end
                        default: `halt
                     endcase                 
                  end
                  
                  6'b101001: begin // pchl
                     pc <= `rHL;
                     `entrypoint
                  end
						
						6'bxxx110: begin // alu imm - adi,aci,aui,abi,ani,xri,ori,cpi
							reg [2:0] op;
							op = opcode[5:3];
							
							unique case (step)
								0: `m_fetch_imm
								1: begin
									alu_op <= op;
									alu_a <= `rA;
									alu_cin <= `fC;
									alu_b <= res;
									`next
								end
								2: begin
									`rA <= alu_out;
									`fC <= alu_car;
									`fZ <= alu_zer;
									`fS <= alu_sgn;
									`fP <= alu_par;
									`fA <= alu_axc;
									`nexti
								end
								default: `halt
							endcase
						end
                  
                  6'b000011, 6'bxxx010: begin // jmp,  jnz,jz,jnc,jc,jpo,jpe,jp,jm
                     reg jmp;
                     reg [2:0] cond;
                     reg cond_val;
                     jmp = opcode[0];
                     cond = opcode[5:4];
                     cond_val = opcode[3];
                     
                     unique case (step)
                        0: `m_fetch_imm2
                        1: begin
                           if (!jmp) begin
                              unique case (cond)
                                 2'b00: jmp = cond_val == `fZ;
                                 2'b01: jmp = cond_val == `fC;
                                 2'b10: jmp = cond_val == `fP;
                                 2'b11: jmp = cond_val == `fS;
                              endcase
                           end
                           
                           if (jmp) begin
                              pc <= `res16;
                              `entrypoint
                           end else begin
                              `nexti
                           end
                        end
                        default: `halt
                     endcase
                  end
						
						6'b001101, 6'bxxx100: begin // call, cnz,cz,cnc,cc,cpo,cpe,cp,cm
							reg call;
							reg [2:0] cond;
							reg cond_val;
							call = opcode[0];
                     cond = opcode[5:4];
                     cond_val = opcode[3];
							
							unique case (step)
								0: `m_fetch_imm2
								1: begin
									`temp16 <= `res16;
								
									if (!call) begin
										unique case (cond)
                                 2'b00: call = cond_val == `fZ;
                                 2'b01: call = cond_val == `fC;
                                 2'b10: call = cond_val == `fP;
                                 2'b11: call = cond_val == `fS;
                              endcase
									end
									
									if (call) begin
										sp = sp - 2;
										`arg16 = sp;
										`res16 = intcyc ? pc : pc + 1;
										
										`push(m_store2)
									end else begin
										`nexti
									end
								end
								2: begin
									pc <= `temp16;
									`entrypoint
								end
								default: `halt
							endcase
						end
						
						6'b001001, 6'bxxx000: begin // ret,  rnz,rz,rnc,rc,rpo,rpe,rp,rm
							reg ret;
							reg [2:0] cond;
							reg cond_val;
							ret = opcode[0];
                     cond = opcode[5:4];
                     cond_val = opcode[3];
							
							unique case (step)
								0: begin
									if (!ret) begin
										unique case (cond)
                                 2'b00: ret = cond_val == `fZ;
                                 2'b01: ret = cond_val == `fC;
                                 2'b10: ret = cond_val == `fP;
                                 2'b11: ret = cond_val == `fS;
                              endcase
									end
									
									if (ret) begin
										`arg16 = sp;
										`push(m_fetch2)
									end else begin
										`nexti
									end
								end
								1: begin
									sp <= sp + 2;
									pc <= `res16;
									`entrypoint
								end
								default: `halt
							endcase
						end
						
						6'bxxx111: begin // rst
							reg [2:0] ex;
							ex = opcode[5:3];
							
							unique case (step)
								0: begin
									sp = sp - 2;
									`arg16 = sp;
									`res16 = intcyc ? pc : pc + 1;
									
									`push(m_store2)
								end
								1: begin
									pc <= {10'b0, ex, 3'b0};
									`entrypoint
								end
								default: `halt
							endcase
						end
						
						default: `halt
               endcase
            end
         endcase
      end
      
      s_halt: begin
         
      end

`define readmem_int(val) if (intcyc_fetchimm) inta <= val; else readmem <= val;

      // arg: arg16=addr; res: res=data
      m_fetch: begin
         unique case (step)
            0: begin
					`readmem_int(1)
               addr <= `arg16;
               `next
            end
            1: begin
               res <= data;
               `readmem_int(0)
					intcyc_fetchimm <= 0;
               `pop
            end
            default: `halt
         endcase
      end
      
		// fetch lo from addr, hi from addr+1
      // arg: arg16=addr; res: res16=data
      m_fetch2: begin
         unique case (step)
            0: begin
               `readmem_int(1)
               addr <= `arg16;
               `next
            end
            1: begin
               res <= data;
               addr <= addr + 1;
               if (intcyc_fetchimm) begin
						inta <= 0; // set inta low for a cycle so device can prepare for next read
						`next
					end else begin
						step <= 3; // skip straight to next read
					end
            end
				2: inta <= 1;  // only for interrupt cycle
            3: begin
               resh <= data;
               `readmem_int(0)
					intcyc_fetchimm <= 0;
               `pop
            end
            default: `halt
         endcase
      end
		
		//arg: arg16=addr, res=data; res: none
		m_store: begin
			unique case (step)
				0: begin
					data_out <= res;
					data_out_ena <= 1;
					addr <= `arg16;
					writemem <= 1;
					`next
				end
				1: begin
					data_out_ena <= 0;
					writemem <= 0;
					`pop
				end
				default: `halt
			endcase
		end
	
		// store lo at addr, hi at addr+1
		//arg: arg16=addr, res16=data; res: none
		m_store2: begin
			unique case (step)
				0: begin
					data_out <= res;
					data_out_ena <= 1;
					addr <= `arg16;
					writemem <= 1;
					`next
				end
				1: begin
					data_out <= resh;
					addr <= addr + 1;
					`next
				end
				2: begin
					data_out_ena <= 0;
					writemem <= 0;
					`pop
				end
				default: `halt
			endcase
		end
      
      default: `halt
   endcase
end

`ifdef PROBE
wire probe_override_reset = probe_source[0];
wire probe_s_reset = probe_source[1];
wire probe_override_clock = probe_source[2];
wire probe_s_clock = probe_source[3];

wire  [15:0] probe_source;
wire [192:0] probe_probe = {
	intcyc_fetchimm, // 137
	inte, // 136
	intcyc, // 135
	inta, // 134
	intr, // 133
	`fC, // 132
	`fP, // 131
	`fA, // 130
	`fZ, // 129
	`fS, // 128
   step_stack[1:0],  //127:126
   state_stack[5:0], //125:120
   sp[15:0],      //119:104
   data[7:0],     //103:96
   opcode[7:0],   //95:88
   pc[15:0],      //87:72
   regs[rB][7:0], //71:64
   regs[rC][7:0], //63:56
   regs[rD][7:0], //55:48
   regs[rE][7:0], //47:40
   regs[rH][7:0], //39:32
   regs[rL][7:0], //31:24
   regs[rM][7:0], //23:16
   regs[rA][7:0], //15:8
   step[1:0],     //7:6
   state[3:0],    //5:2
   clock,         //1
   reset,         //0
};

cpuprobe cpuprobe (
   .source(probe_source),
   .probe(probe_probe)
// .source_clk(~in_clock)
);
`endif

endmodule

enum bit [3:0] {
   aADD, aADC, aSUB, aSBB, aAND, aXOR, aOR, aCMP
} alu_ops;

module alu(
   input [2:0] op,
   input [7:0] a,
   input [7:0] b,

   input cin,
   
   input ena,
   
   output tri0 [7:0] out,

   output car,
   output zer,
   output sgn,
   output par,
   output axc
);

wire [7:0] res;

assign out = ena ? res : 8'bZ;

always_comb begin
	reg [7:0] resi;
   car = 0;
   axc = 0;

   unique case (op)
      aADD: begin
         {car, resi} = a + b;
         axc = (a[3:0] + b[3:0]) >> 4;
      end
      
      aADC: begin
         {car, resi} = a + b + cin;
         axc = (a[3:0] + b[3:0] + cin) >> 4;
      end
      
      aSUB, aCMP: begin
         {car, resi} = a - b;
         axc = (a[3:0] - b[3:0]) >> 4;
      end
      
      aSBB: begin
         {car, resi} = a - b - cin;
         axc = (a[3:0] - b[3:0] - cin) >> 4;
      end
      
      aAND: resi = a & b;
      aXOR: resi = a ^ b;
      aOR:  resi = a | b;
   endcase

   zer = ~|resi;
   sgn = resi[7];
   par = ~^resi;
	
	res = op == aCMP ? a : resi;
end

endmodule
