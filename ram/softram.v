module softram #(
   parameter DATA_WIDTH=8,
   parameter ADDR_WIDTH=8,
   parameter RAM_IMG="ram_img.txt"
)(
   inout [(DATA_WIDTH-1):0] data,
   
   input [(ADDR_WIDTH-1):0] addr,
   
   input re, // read enable
   input we, // write enable
   
   input wa // write address
);

   reg [DATA_WIDTH-1:0] ram[2**ADDR_WIDTH-1:0];
   reg [ADDR_WIDTH-1:0] addr_reg;
   
   initial begin
      $readmemh(RAM_IMG, ram);
   end
   
   always @(*) begin
      if (wa)
         addr_reg <= addr;
      if (we)
         ram[addr_reg] <= data;
   end
   
   assign data = re ? ram[addr_reg] : {DATA_WIDTH{1'bZ}};
   
endmodule
