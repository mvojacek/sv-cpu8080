module main(
   input  board_clk, board_reset,
   
   input  [3:0] board_switches,
   input        board_infra,
   
   output [3:0] board_3461BS_D,
   output [6:0] board_3461BS_S,
   output       board_3461BS_DP,
   
   output [3:0] board_leds,
   output       board_buzzer
);

wire reset = ~board_reset;

// ======
// Clocks
// ======

wire clk50M = board_clk;
wire clk1M;
wire clkRTC;
wire clk1H;
wire clk1300M;
wire clkCPU;
wire clkPlex;
wire clkInt;
wire [15:0] rtcCounter;

clkdivider clkdivider(
   .clk50M(clk50M),
   .clk1M(clk1M), .clkRTC(clkRTC), .clk1H(clk1H), .clk1300M(clk1300M),
   .clkCPU(clkCPU), .clkPlex(clkPlex), .clkInt(clkInt),
	.rtcCounter(rtcCounter)
);

// ======
// Memory
// ======

wire mem_clock = ~cpu_clock;
wire [15:0] mem_addr;
wire [7:0] mem_in;
wire mem_re, mem_we;
wire [7:0] mem_out;

memory memory(
   .addr(mem_addr),
   .clock(mem_clock),
   .in(mem_in),
   .re(mem_re),
   .we(mem_we),
   .out(mem_out)
);

// ======
// CPU
// ======

wire cpu_clock = clkCPU;
wire cpu_reset = reset;
wire [15:0] cpu_addr;
wire [7:0]  cpu_data;
wire cpu_readmem, cpu_writemem;
wire cpu_readio, cpu_writeio;
wire cpu_intr, cpu_inta;
wire cpu_hold, cpu_holda;

cpu cpu(
   .in_clock(cpu_clock), .in_reset(cpu_reset),
   
   .addr(cpu_addr),
   .data(cpu_data),
   
   .readmem(cpu_readmem),
   .writemem(cpu_writemem),
   
   .readio(cpu_readio),
   .writeio(cpu_writeio),
	
	.intr(cpu_intr),
	.inta(cpu_inta),
		
	.hold(cpu_hold),
	.holda(cpu_holda)
);

// ======
// Devices
// ======

wire dev_clock = ~cpu_clock;
wire dev_seg7_plex_clock = clkPlex;
wire dev_int_clock = clkInt;
wire [7:0] dev_addr;
wire [7:0] dev_data;
wire dev_read, dev_write;

devices devices(
   .clock(dev_clock),
   .seg7_plex_clock(dev_seg7_plex_clock),
	.int_clock(dev_int_clock),
	.rtcCounter(rtcCounter),
	
   .addr(dev_addr),
   .data(dev_data),
   .read(dev_read),
   .write(dev_write),
	
	.cpu_intr(cpu_intr),
	.cpu_inta(cpu_inta),
   
   .board_switches(board_switches),
   .board_3461BS_D(board_3461BS_D),
   .board_3461BS_S(board_3461BS_S),
   .board_3461BS_DP(board_3461BS_DP),
   .board_leds(board_leds),
   .board_buzzer(board_buzzer),
   .board_infra(board_infra)
);

// ======
// Interconnects
// ======

tri0 [7:0] data;
assign mem_in = data;
assign data = mem_out;
assign data = cpu_data;

assign mem_addr = cpu_addr;
assign mem_re = cpu_readmem;
assign mem_we = cpu_writemem;

assign dev_addr = cpu_addr[7:0];
assign dev_data = data;
assign data = dev_data;
assign dev_read = cpu_readio;
assign dev_write = cpu_writeio;

endmodule
