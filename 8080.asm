#cpudef
{
    #bits 8
	
	nop      -> 0x00
	inr {r}  -> 0b00 @ r[2:0] @ 0b100
	dcr {r}  -> 0b00 @ r[2:0] @ 0b101
	stax B   -> 0x02
	stax D   -> 0x12
	ldax B   -> 0x0A
	ldax D   -> 0x1A
	inx B    -> 0x03
	inx D    -> 0x13
	inx H    -> 0x23
	inx SP   -> 0x33
	dcx B    -> 0x0B
	dcx D    -> 0x1B
	dcx H    -> 0x2B
	dcx SP   -> 0x3B
	dad B    -> 0x09
	dad D    -> 0x19
	dad H    -> 0x29
	dad SP   -> 0x39
	push B   -> 0xC5
	push D   -> 0xD5
	push H   -> 0xE5
	push PSW -> 0xF5
	pop B    -> 0xC1
	pop D    -> 0xD1
	pop H    -> 0xE1
	pop PSW  -> 0xF1
	lxi B, {w}  -> 0x01 @ w[7:0] @ w[15:8]
	lxi D, {w}  -> 0x11 @ w[7:0] @ w[15:8]
	lxi H, {w}  -> 0x21 @ w[7:0] @ w[15:8]
	lxi SP, {w} -> 0x31 @ w[7:0] @ w[15:8]
	mvi {r}, {b} -> 0b00 @ r[2:0] @ 0b110 @ b[7:0]
	in {a}  -> 0xDB @ a[7:0]
	out {a} -> 0xD3 @ a[7:0]
	hlt  -> 0x76
	pchl -> 0xE9
	xchg -> 0xEB
	xthl -> 0xE3
	sphl -> 0xF9
	jmp {a} -> 0xC3 @ a[7:0] @ a[15:8]
	jnz {a} -> 0xC2 @ a[7:0] @ a[15:8]
	jnc {a} -> 0xD2 @ a[7:0] @ a[15:8]
	jpo {a} -> 0xE2 @ a[7:0] @ a[15:8]
	jp  {a} -> 0xF2 @ a[7:0] @ a[15:8]
	jz  {a} -> 0xCA @ a[7:0] @ a[15:8]
	jc  {a} -> 0xDA @ a[7:0] @ a[15:8]
	jpe {a} -> 0xEA @ a[7:0] @ a[15:8]
	jm  {a} -> 0xFA @ a[7:0] @ a[15:8]
	call {a} -> 0xCD @ a[7:0] @ a[15:8]
	cnz  {a} -> 0xC4 @ a[7:0] @ a[15:8]
	cnc  {a} -> 0xD4 @ a[7:0] @ a[15:8]
	cpo  {a} -> 0xE4 @ a[7:0] @ a[15:8]
	cp   {a} -> 0xF4 @ a[7:0] @ a[15:8]
	cz   {a} -> 0xCC @ a[7:0] @ a[15:8]
	cc   {a} -> 0xDC @ a[7:0] @ a[15:8]
	cpe  {a} -> 0xEC @ a[7:0] @ a[15:8]
	cm   {a} -> 0xFC @ a[7:0] @ a[15:8]
	ret -> 0xC9
	rnz -> 0xC0
	rnc -> 0xD0
	rpo -> 0xE0
	rp  -> 0xF0
	rz  -> 0xC8
	rc  -> 0xD8
	rpe -> 0xE8
	rm  -> 0xF8
	add {r} -> 0b10 @ 0b000 @ r[2:0]
	adc {r} -> 0b10 @ 0b001 @ r[2:0]
	sub {r} -> 0b10 @ 0b010 @ r[2:0]
	sbb {r} -> 0b10 @ 0b011 @ r[2:0]
	ana {r} -> 0b10 @ 0b100 @ r[2:0]
	xra {r} -> 0b10 @ 0b101 @ r[2:0]
	ora {r} -> 0b10 @ 0b110 @ r[2:0]
	cmp {r} -> 0b10 @ 0b111 @ r[2:0]
	adi {b} -> 0xC6 @ b[7:0]
	aci {b} -> 0xCE @ b[7:0]
	sui {b} -> 0xD6 @ b[7:0]
	sbi {b} -> 0xDE @ b[7:0]
	ani {b} -> 0xE6 @ b[7:0]
	xri {b} -> 0xEE @ b[7:0]
	ori {b} -> 0xF6 @ b[7:0]
	cpi {b} -> 0xFE @ b[7:0]
	sta {a}  -> 0x32 @ a[7:0] @ a[15:8]
	lda {a}  -> 0x3A @ a[7:0] @ a[15:8]
	shld {a} -> 0x22 @ a[7:0] @ a[15:8]
	lhld {a} -> 0x2A @ a[7:0] @ a[15:8]
	mov {d}, {s} -> 0b01 @ d[2:0] @ s[2:0]
	rlc -> 0x07
	ral -> 0x17
	daa -> 0x27
	stc -> 0x37
	rrc -> 0x0F
	rar -> 0x1F
	cma -> 0x2F
	cmc -> 0x3F
	ei  -> 0xFB
	di  -> 0xF3
	rst {vec} -> 0b11 @ vec[2:0] @ 0b111
}

; IO map
seg7lo = 0x00
seg7hi = 0x01
seg7crl = 0x02
leds = 0x03
switches = 0x04
buzzer = 0x05
bufferFreq = 0x06
xx = 0x07
int0 = 0x08
int2 = 0x09
int4 = 0x0A
int5 = 0x0B

; registers
B = 0b000
C = 0b001
D = 0b010
E = 0b011
H = 0b100
L = 0b101
M = 0b110
A = 0b111

#bankdef "int0"
{
	#addr 0x00
	#size 0x8
	#outp 0x00
	#fill
}

#bankdef "int1"
{
	#addr 0x08
	#size 0x8
	#outp 0x08
	#fill
}

#bankdef "int2"
{
	#addr 0x10
	#size 0x8
	#outp 0x10
	#fill
}

#bankdef "int3"
{
	#addr 0x18
	#size 0x8
	#outp 0x18
	#fill
}

#bankdef "int4"
{
	#addr 0x20
	#size 0x8
	#outp 0x20
	#fill
}

#bankdef "int5"
{
	#addr 0x28
	#size 0x8
	#outp 0x28
	#fill
}

#bankdef "int6"
{
	#addr 0x30
	#size 0x8
	#outp 0x30
	#fill
}

#bankdef "int7"
{
	#addr 0x38
	#size 0x8
	#outp 0x38
	#fill
}

#bankdef "prog"
{
	#addr 0x0040
	#size 0x0FC0 ; 0x1000 - 0x40
	#outp 0x0040
	#fill
}

#bankdef "ram"
{
	#addr 0x1000
	#size 0x1000
	#outp 0x1000
	#fill
}

#bankdef "stack"
{
	#addr 0x2000
	#size 0x1000
	#outp 0x2000
	#fill
}

; ======
; PROGRAM START
; ======

#bank "int0" ; startup trampoline

di
jmp start

#bank "stack"

#res 0x1000 - 2
stackend:

#bank "prog"

start:

init_stack:
	lxi SP, stackend

init_7seg:
	mvi A, 0b01001111
	out seg7crl
	mvi A, 0
	out seg7lo
	out seg7hi
	
init_intmgr:
	mvi A, 0b01010011 ; switch 1 enabled on vector 2, switch 0 enabled on vector 1
	;mvi A, 0 ; disable both interrupts
	out int0

;jmp fibbonacci
jmp intcounter
;jmp stacktests


#bank "prog"

stacktests:
	mvi A, 1
	lxi B, 0x0203
	lxi D, 0x0405
	lxi H, 0x0607

	push PSW
	push B
	push D
	push H

	hlt

	mvi A, 0xFF
	lxi B, 0xFFFF
	lxi D, 0xFFFF
	lxi H, 0xFFFF

	pop H
	pop D
	pop B
	pop PSW

	hlt
	

#bank "int1" ; switch 0 interrupt routine

push B
mvi B, 1 ; will add 1
jmp intcounter_svc

#bank "int2" ; switch 1 interrupt routine

push B
mvi B, -1 ; will subtract 1
jmp intcounter_svc

#bank "ram"
intcounter_val:
	#d8 0x80

#bank "prog"

intcounter_svc:
	push PSW
	
	; skip action on switch release
	in switches
	cpi 0
	jz .early
	
	push H
	
	; add B to counter
	lxi H, intcounter_val
	mov A, M
	add B
	mov M, A
	out seg7lo
	
	.end:
	pop H
	.early:
	pop PSW
	pop B
	ei
	ret

intcounter:
	mvi A, 0x00
	ei
	.loop:
		inr A
		out seg7hi
		; wait
		mvi B, 255
		.wait1:
			dcr B
			inr B
			dcr B
			inr B
			dcr B
			inr B
			dcr B
			jnz .wait1
		jmp .loop

playground:
	mvi C, 0xA0
	
	.loop:
		stax B
		out seg7lo
		inr A
		inr C
		jmp .loop
	hlt

; must be odd, count is checked only every 2 iterations
fibcount = 23

; amount of iterations to wait when displaying results
displaywait = 255

#bank "ram"
fibspace:
	#d16 1
fibstart:
	#res fibcount*2-2
fibend:
	#d16 0xABCD

#bank "prog"
fibbonacci:
	lxi H, fibstart ; load destination into HL
	lxi B, 1 ; load 1 into BC
	lxi D, 1 ; load 1 into DE
	.loop:
		; store BC at next HL
		mov M, B
		inx H
		mov M, C
		inx H
		; output BC
		mov A, C
		out seg7lo
		mov A, B
		out seg7hi
		; wait
;		mvi A, displaywait
;		.wait1:
;			dcr A
;			inr A
;			dcr A
;			inr A
;			dcr A
;			inr A
;			dcr A
;			jnz .wait1
		; add BC to DE
		xchg	 ; swap HL and DE
		dad B	 ; add BC to HL
		xchg	 ; swap HL and DE back
		; store DE at next HL
		mov M, D
		inx H
		mov M, E
		inx H
		; output DE
		mov A, E
		out seg7lo
		mov A, D
		out seg7hi
		; wait
;		mvi A, displaywait
;		.wait2:
;			dcr A
;			inr A
;			dcr A
;			inr A
;			dcr A
;			inr A
;			dcr A
;			jnz .wait2
		; add DE to BC
		mov A, C ; C to acc
		add E ; add E to it, sets carry
		mov C, A ; move C back
		mov A, B ; B to acc
		adc D ; add D to it, with carry
		mov B, A ; move B back
		; repeat if not done
		mvi A, fibend[15:8] ; load high addr for comparison
		cmp H ; compare: A - H
		jnz .loop ; if A != H, repeat
		mvi A, fibend[7:0] ; high are equal, load low addr for comparison
		cmp L ; compare: A - L
		jnz .loop ; if A != L, repeat
	; show were done
	mvi A, 0xAB
	out seg7hi
	mvi A, 0xCD
	out seg7lo
	mvi A, 0xFF
	out seg7crl
	; halt
	hlt
	
