#cpudef
{
    #bits 8
    
	mov {ra}, [cs@#{imm}] -> (0x00 + ra[1:0])[7:0] @ imm[7:0]
	mov [cs@#{imm}], {ra} -> (0x04 + ra[1:0])[7:0] @ imm[7:0]
	mov {ra}, [cs@rmem] -> (0x08 + ra[1:0])[7:0]
	mov [cs@rmem], {ra} -> (0x0C + ra[1:0])[7:0]
	
	mov {ra}, {rb} -> (0x10 + (rb[1:0] @ ra[1:0])[3:0])[7:0]
	mov {ra}, #{imm} -> (0x28 + ra[1:0])[7:0] @ imm[7:0]
	
	nop -> 0x32
	
	rjmp {ra} -> (0x20 + ra[1:0])[7:0]
	jmp cs@{ra} -> (0x24 + ra[1:0])[7:0]
	rjmp #{imm} -> 0x30 @ imm[7:0]
	jmp cs@#{imm} -> 0x31 @ imm[7:0]
	
	add {ra}, #{imm} -> (0x90 + ra[1:0])[7:0] @ imm[7:0]
	mul {ra}, #{imm} -> (0x9C + ra[1:0])[7:0] @ imm[7:0]
	and {ra}, #{imm} -> (0x94 + ra[1:0])[7:0] @ imm[7:0]
	or  {ra}, #{imm} -> (0x98 + ra[1:0])[7:0] @ imm[7:0]
	
	add {ra}, {rb} -> (0x40 + (rb[1:0] @ ra[1:0])[3:0])[7:0]
	mul {ra}, {rb} -> (0x50 + (rb[1:0] @ ra[1:0])[3:0])[7:0]
	and {ra}, {rb} -> (0x60 + (rb[1:0] @ ra[1:0])[3:0])[7:0]
	or  {ra}, {rb} -> (0x70 + (rb[1:0] @ ra[1:0])[3:0])[7:0]
	xor {ra}, {rb} -> (0x80 + (rb[1:0] @ ra[1:0])[3:0])[7:0]
	neg {ra} -> (0x2C + ra[1:0])[7:0]
	
	seg7l {ra} -> (0xA0 + ra[1:0])[7:0]
	seg7h {ra} -> (0xA4 + ra[1:0])[7:0]
	
	cs {ra} -> (0xA8 + ra[1:0])[7:0]
	jd {ra} -> (0xAC + ra[1:0])[7:0]
	
	flgclr -> 0x33
	
	jd #{imm} -> 0xB0 @ imm[7:0]
	jmpz cs@jd -> 0xB1
	jmpc cs@jd -> 0xB2
	jmpn cs@jd -> 0xB3
	sleep #{imm} -> 0xB4 @ imm[7:0]
	rjmpz jd -> 0xB5
	rjmpc jd -> 0xB6
	rjmpn jd -> 0xB7
	cs #{imm} -> 0xB8 @ imm[7:0]
}

ra = 0b00
rb = 0b01
rmem = 0b10
acc = 0b11

#bankdef "00"
{
    #addr 0x0000
    #size 0x100
    #outp 0x0
}


#bank "00"
#addr 0x00
start:
	cs #0
	mov ra, #0xA
	jmp cs@#stop
	
	jmp cs@#countmem[7:0]

countmem:
	mov acc, #0xA
	
	jd #.next
	.loop:
		mov rmem, acc
		mov [cs@rmem], rmem
		add rmem, -0x1
		jmpz cs@jd
		jmp cs@#.loop
	.next:
		flgclr
		jmp cs@#stop
stop:
	jmp cs@#stop
		